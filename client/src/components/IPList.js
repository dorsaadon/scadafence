import React, { Component } from "react";
import PropTypes from 'prop-types';

import IPItem from './IPItem';

class IPList extends Component {
    render() {
        return this.props.ipList.map((ipItem) => (
            <IPItem ipItem = {ipItem} delIPItem={this.props.delIPItem}/>
        ));
    }
}

// PropTypes
IPList.propTypes = {
    ipList: PropTypes.array.isRequired,
    isInternal: PropTypes.func.isRequired,
    delIPItem: PropTypes.func.isRequired
}

export default IPList;