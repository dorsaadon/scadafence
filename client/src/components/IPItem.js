import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class IPItem extends Component {
    getStyle = () => {
        return {
            background: '#F4F4F4',
            padding: '10px',
            borderBottom: '1px #ccc dotted'
        }
    };

    render() {
        const {id, from, to, isInternal } = this.props.ipItem;
        var internalExternal = isInternal ? "Internal" : "External";
        return (
            <div style={ this.getStyle() }>
                <p>
                    {from} - {to}
                    <input
                    type="checkbox"
                    checked={isInternal}
                    />
                    {internalExternal}
                    <button onClick={this.props.delIPItem.bind(this, id)} style={{ float: 'right' }}>
                        Delete
                    </button>
                </p>
            </div>
        )
    }
}

// PropTypes
IPItem.propTypes = {
    IPList: PropTypes.object.isRequired,
    isInternal: PropTypes.func.isRequired,
    delIPItem: PropTypes.func.isRequired
}


export default IPItem;