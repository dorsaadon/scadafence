import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SimpleReactValidator from 'simple-react-validator';

export class AddIPRange extends Component {
    
    constructor() {
        super();
        this.validator = new SimpleReactValidator({
            validators: {
              ip: {  // name the rule
                message: 'The :attribute must be a valid IP address.',
                rule: (val, params, validator) => {
                  return validator.helpers.testRegex(val,/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/i) && params.indexOf(val) === -1
                },
                required: true  // optional
              },
              ipRange: {
                  message: 'The end range must be bigger than the starting range.',
                  rule: (val,params,validator) => {
                    let from = this.props.convert(val[0]);
                    let to = this.props.convert(val[1]);
                    return (from < to)
                  },
                  required: true
              }
            }
          })
    }

    state = {
        ipFrom: '',
        ipTo: '',
        isInternal: false
    }

    onSubmit = (e) => {
        e.preventDefault();
        if (this.validator.allValid()) {
        this.props.addIPRange(this.state.ipFrom,
                           this.state.ipTo,
                           this.state.isInternal);
        this.setState({ ipFrom: '', 
                        ipTo: '',
                        isInternal: false});
        }
        else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    onChangeFrom = (e) => this.setState({ ipFrom: e.target.value});
    onChangeTo = (e) => this.setState({ipTo: e.target.value});
    onChangeChecked = (e) => this.setState({isInternal: !this.state.isInternal});

    render() {
        return (
            <form onSubmit={this.onSubmit} style={{display: 'flex'}}>
                IP From: 
                <input 
                    type="text" 
                    name="ipFrom"
                    style={{flex: '10', padding: '5px'}}
                    placeholder="1.1.1.1" 
                    value={this.state.ipFrom}
                    onChange={this.onChangeFrom}
                />
                {this.validator.message('ipFrom', this.state.ipFrom, 'required|ip:127.0.0.1')}
                &nbsp;
                IP To:
                <input 
                    type="text" 
                    name="ipTo"
                    style={{flex: '10', padding: '5px'}}
                    placeholder="255.255.255.255" 
                    value={this.state.ipTo}
                    onChange={this.onChangeTo}
                />
                {this.validator.message('ipTo', this.state.ipTo, 'required|ip:127.0.0.1')}
                {this.validator.message('ipRange', [this.state.ipFrom, this.state.ipTo], 'required|ipRange:127.0.0.1')}
                <input
                    type="checkbox"
                    name="isInternal"
                    checked={this.state.isInternal}
                    onChange={this.onChangeChecked}
                />
                <span>Is Internal?</span>
                <input 
                    type="submit"
                    value="Submit"
                    className="btn"
                    style= {{flex: '1'}}
                />
            </form>
        )
    }
}

// PropTypes
AddIPRange.propTypes = {
    ipFrom: PropTypes.func.isRequired,
    ipTo: PropTypes.func.isRequired
}

export default AddIPRange;
