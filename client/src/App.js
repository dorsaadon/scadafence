// Importing Modules
import React, { Component } from 'react';
import axios from 'axios';

// import uuid from 'uuid';  // Not Used Anymore

// Importing React-Router
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Importing Components
import IPList from './components/IPList';
import Header from './components/layout/Header';
import AddIPRange from './components/AddIPRange';


import './App.css';

class App extends Component {
  state = {
    ipList: []
  }

  componentDidMount() {
    
    axios.get('http://localhost:8000/')
      .then((res) => { 
        this.setState({ ipList: res.data.ips });
        console.log(res.data);
    }).catch((err) => {
      console.log(err);
    });
  }

  convert = (ip) => {
    function pad(n) {
      return (n.length < 3) ? pad('0' + n) : n;
  }
    return parseInt((ip + '').split('.').map(function (el) {
       return pad(el);
    }).join(''), 10);
}


  // Delete IP Range
  delIPItem = (id) => {
    axios.delete('http://localhost:8000/ips/' + id)
      .then(res => this.setState({ ipList: [...this.state.ipList.filter(ipItem => ipItem.id !== id)]}));
  }

  addIPRange = (ipFrom, ipTo, isInternal) => {
    axios.post('http://localhost:8000/ips', {
      ipFrom: ipFrom,
      ipTo: ipTo,
      isInternal: isInternal
    })
      .then(res => {
        if (res.data.message != "OK"){
          alert(res.data.message);
        } else {
         this.setState({ipList: res.data.ips})
      }
      }).catch((err) => {
        alert(err);
        console.log(err);
      });
  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="container">
            <Header />
            <br />
            <Route exact path="/" render={props => (
              <React.Fragment>
                <AddIPRange addIPRange={this.addIPRange} convert={this.convert} />
                <IPList ipList={this.state.ipList} isInternal = {this.isInternal} delIPItem={this.delIPItem}/>
              </React.Fragment>
            )} />

          </div>
        </div>
      </Router>
    );
  }
}

export default App;