var exports = module.exports = {};
//const check = require('express-validator');
//const { body } = require('express-validator/check');

function validateIPAddress(ip) {
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip)) {  
        return (true)  
    }
    else {
        return false;
    }
}

function pad(n) {
    return (n.length < 3) ? pad('0' + n) : n;
}

// return a number with each part of the IP padded out to 3 digits
function convert(ip) {
    return parseInt(ip.split('.').map(function (el) {
        return pad(el);
    }).join(''), 10);
}

// check the number against the range
function check(range, ip) {
    ip = convert(ip);
    return ip >= range[0] && ip <= range[1];
}





function checkIfAlreadyInRange(ipFrom, ipTo, ipList) {
    var range = [ipFrom, ipTo].map(function (el) {
        return convert(el);
    });
    var isInRange = false;
    
    for (let i = 0; i < ipList.length; i++) {
        if (check(range, ipList[i].from) || (check(range, ipList[i].to))) {
            return true;
        };
    };
    return isInRange;
}

exports.validate = (data, ipList) => { 
    if (validateIPAddress(data.ipFrom) ||
    validateIPAddress(data.ipTo)) {
        if (checkIfAlreadyInRange(data.ipFrom, data.ipTo, ipList)) {
            throw new Error("The range is already a part of another range");
        }
    } else {
        throw new Error("One or both of the ips is Invalid");
    }

    return "OK";
}

/*    return([
        body('from', 'Invalid start of IP range').exists().custom((val) => {
            return (validateIPAddress(val));
        }),
        body('to', 'Invalid end of IP range').exists().custom((val) => {
            return (validateIPAddress(val));
        }),
        body('isInternal').optional().isBoolean()
    ])
};*/