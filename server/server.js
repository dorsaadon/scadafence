// Create express app
var express = require("express")
var app = express()
const cors = require('cors')
const low = require('lowdb')
const shortid = require('shortid')
const FileSync = require('lowdb/adapters/FileSync')
var bodyParser = require("body-parser")
const ipsModel = require("./models/ips")

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const adapter = new FileSync('db.json')
const db = low(adapter)

app.use(cors())
// Server port
var HTTP_PORT = 8000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    var output = db.get('ips').value();
    res.json({message: "Ok",
             ips: output})
})

app.delete("/ips/:id", (req, res, next) => {
  db.get('ips')
  .remove({id:req.params.id})
  .write();
  let output = db.get('ips').value();
  res.json({message: 'Delete OK',
            ips: output});
})

app.post("/ips/", (req,res,next) => {
  let data = req.body;
  try {
    ipsModel.validate(data, db.get('ips').value());
    let output = db.get('ips')
              .push({id: shortid.generate(), from:data.ipFrom, to:data.ipTo, isInternal:data.isInternal})
              .write();
    res.json({message: msg,
            ips: output});
  }
  catch (msg) {
    res.json({message: msg.message})
    //res.send(500, {error: msg});
  };
})

// Insert here other API endpoints

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});

// Set some defaults (required if your JSON file is empty)
db.defaults({ips: []})
  .write()

// Add a post
//db.get('ips')
//  .push({ from: '1.1.1.1', to: '255.255.255.255', internal:'true'})
//  .write()

// Set a user using Lodash shorthand syntax
//db.set('user.name', 'typicode')
//  .write()
  
// Increment count
//db.update('count', n => n + 1)
//  .write()